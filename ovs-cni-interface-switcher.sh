#!/bin/bash

set -x

# Function to display help
show_help() {
    echo "Usage: $0 [-c container_name]"
    echo
    echo "Options:"
    echo "  -c container_name     Set option B with a value."
    echo "  -h                    Show this help message."
}

# Initialize variables
container_name=""
period=3

error_message="could not open network device test-port (No such device)"
# Parse options
while getopts ":c:h" opt; do
    case ${opt} in
        c )
            container_name=$OPTARG
            ;;
        h )
            show_help
            exit 0
            ;;
        \? )
            echo "Invalid option: -$OPTARG" 1>&2
            show_help
            exit 1
            ;;
        : )
            echo "Option -$OPTARG requires an argument." 1>&2
            show_help
            exit 1
            ;;
    esac
done
shift $((OPTIND -1))

# Check if required arguments are provided
if [ -z "$container_name" ]; then
    echo "Error: container name is required, please provide it using -c flag."
    show_help
    exit 1
fi

switch_netns=$(docker inspect --format '{{.State.Pid}}' ${container_name})

exec_command="docker exec -t ${container_name} "

while true; do
      for port in $(docker exec -t ${container_name} ovs-vsctl -f json list port | jq -r .data[][12]);do
        if [[ "$(docker exec -t ${container_name} ovs-vsctl -f json list interface ${port} | jq -r .data[0][12])" == "could not open network device ${port} (No such device)" ]];then
          ip l set ${port} netns ${switch_netns}
          ${exec_command} ip l set ${port} up
        fi
      done
    sleep ${period}
done

